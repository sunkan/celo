<?php

namespace Celo\Tests;

use Celo\Exception\CorruptDataException;
use Celo\Exception\LockToException;
use Celo\Exception\MissingDataException;
use Celo\Exception\NotValidException;
use Celo\Exception\TokenNotFound;
use Celo\NativeSession;
use Celo\SessionInterface;
use Celo\TokenGenerator;
use Celo\TokenInterface;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

class TokenGeneratorTest extends TestCase
{
    private function getSession(&$session): SessionInterface
    {
        return new NativeSession($session);
    }

    private function getRequest(string $uri = '/test-uri', ?array $body = null, bool $delete = false): ServerRequestInterface
    {
        $request = Mockery::mock(ServerRequestInterface::class);
        $request->shouldReceive('getServerParams')->andReturn(['REMOTE_ADDR'=>'10.0.0.1']);

        if (!is_null($body)) {
            if ($delete) {
                $uriMock = Mockery::mock(UriInterface::class);
                $uriMock->shouldReceive('__toString')->andReturn($uri);
                $uriMock->shouldReceive('withQuery')->andReturn($uriMock);

                $request->shouldReceive('getUri')->andReturn($uriMock);
                $request->shouldReceive('getQueryParams')->andReturn($body);
                $request->shouldReceive('getParsedBody')->andReturn([]);
                $request->shouldReceive('getMethod')->andReturn('DELETE');
                $request->shouldReceive('withQuery')->andReturn($request);
                $request->shouldReceive('withUri')->andReturn($request);
            } else {
                $request->shouldReceive('getMethod')->andReturn('POST');
                $request->shouldReceive('getParsedBody')->andReturn($body);
            }
        }
        if (!$delete) {
            $uriMock = Mockery::mock(UriInterface::class);
            $uriMock->shouldReceive('__toString')->andReturn($uri);
            $request->shouldReceive('getUri')->andReturn($uriMock);
        }


        return $request;
    }


    public function testCreateGenerator()
    {
        $sessionData = [];

        $generator = new TokenGenerator($this->getSession($sessionData), $this->getRequest());

        $this->assertInstanceOf(TokenGenerator::class, $generator);
        $this->assertTrue(isset($sessionData['CSRF']));
    }

    public function testGetToken()
    {
        $sessionData = [];
        $request = $this->getRequest();

        $generator = new TokenGenerator($this->getSession($sessionData), $request);

        $token = $generator->getToken('/dest-uri');

        $this->assertInstanceOf(TokenInterface::class, $token);

        $this->assertCount(1, $sessionData['CSRF']);
    }

    public function testValidateToken()
    {
        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);

        $token = $generator->getToken((string)$destUri);

        $this->assertInstanceOf(TokenInterface::class, $token);

        $this->assertCount(1, $sessionData['CSRF']);

        $request = $this->getRequest($destUri, [
            TokenInterface::FORM_TOKEN => $token->getFormTokenValue(),
            TokenInterface::FORM_INDEX => $token->getIndex()
        ]);

        $validatedToken = $generator->validate($request);
        $this->assertInstanceOf(TokenInterface::class, $validatedToken);

        $this->assertCount(0, $sessionData['CSRF']);
    }

    public function testValidateDeleteRequestMethod()
    {
        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);

        $token = $generator->getToken((string)$destUri);

        $this->assertInstanceOf(TokenInterface::class, $token);

        $this->assertCount(1, $sessionData['CSRF']);

        $request = $this->getRequest($destUri, [
            TokenInterface::FORM_TOKEN => $token->getFormTokenValue(),
            TokenInterface::FORM_INDEX => $token->getIndex()
        ], true);

        $validatedToken = $generator->validate($request);
        $this->assertInstanceOf(TokenInterface::class, $validatedToken);

        $this->assertCount(0, $sessionData['CSRF']);
    }

    public function testValidateTokenExpire()
    {
        $this->expectException(TokenNotFound::class);
        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);

        $token = $generator->getLimitedToken((string)$destUri, 1);

        $this->assertInstanceOf(TokenInterface::class, $token);
        $this->assertCount(1, $sessionData['CSRF']);

        sleep(2);

        $request = $this->getRequest($destUri, [
            TokenInterface::FORM_TOKEN => $token->getFormTokenValue(),
            TokenInterface::FORM_INDEX => $token->getIndex()
        ]);

        $generator->validate($request);
    }

    public function testValidateTokenWithExpire()
    {
        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);

        $token = $generator->getLimitedToken((string)$destUri, 1);

        $this->assertInstanceOf(TokenInterface::class, $token);
        $this->assertCount(1, $sessionData['CSRF']);

        sleep(1);

        $request = $this->getRequest($destUri, [
            TokenInterface::FORM_TOKEN => $token->getFormTokenValue(),
            TokenInterface::FORM_INDEX => $token->getIndex()
        ]);

        $validatedToken = $generator->validate($request);
        $this->assertInstanceOf(TokenInterface::class, $validatedToken);

        $this->assertCount(0, $sessionData['CSRF']);
    }

    public function testCorruptDataException()
    {
        $this->expectException(CorruptDataException::class);

        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);
        $request = $this->getRequest($destUri, [
            TokenInterface::FORM_TOKEN => 1,
            TokenInterface::FORM_INDEX => 2
        ]);

        $generator->validate($request);
    }

    public function testMissingDataException()
    {
        $this->expectException(MissingDataException::class);

        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);
        $request = $this->getRequest($destUri, []);

        $generator->validate($request);
    }

    public function testTokenNotFound()
    {
        $this->expectException(TokenNotFound::class);

        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);
        $request = $this->getRequest($destUri, [
            TokenInterface::FORM_TOKEN => "1",
            TokenInterface::FORM_INDEX => "2"
        ]);

        $generator->validate($request);
    }

    public function testWrongDestination()
    {
        $this->expectException(LockToException::class);

        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);

        $token = $generator->getToken((string)$destUri);

        $this->assertInstanceOf(TokenInterface::class, $token);

        $this->assertCount(1, $sessionData['CSRF']);

        $request = $this->getRequest('wrong-uri', [
            TokenInterface::FORM_TOKEN => $token->getFormTokenValue(),
            TokenInterface::FORM_INDEX => $token->getIndex()
        ]);

        $generator->validate($request);
    }

    public function testInvalidToken()
    {
        $this->expectException(NotValidException::class);

        $sessionData = [];
        $request = $this->getRequest();

        $destUri = '/dest-uri';

        $generator = new TokenGenerator($this->getSession($sessionData), $request);

        $token = $generator->getToken((string)$destUri);

        $this->assertInstanceOf(TokenInterface::class, $token);

        $this->assertCount(1, $sessionData['CSRF']);

        $request = $this->getRequest($destUri, [
            TokenInterface::FORM_TOKEN => "wrong-token",
            TokenInterface::FORM_INDEX => $token->getIndex()
        ]);

        $generator->validate($request);
    }
}
