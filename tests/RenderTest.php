<?php

namespace Celo\Tests;

use Celo\NativeSession;
use Celo\Renderer;
use Celo\SessionInterface;
use Celo\TokenGenerator;
use Celo\TokenInterface;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\ServerRequestFactory;

class RenderTest extends TestCase
{
    private function getSession(&$session): SessionInterface
    {
        return new NativeSession($session);
    }

    public function testGetToken()
    {
        $request = Mockery::mock(ServerRequestInterface::class);
        $request->shouldReceive('getServerParams')->andReturn(['REMOTE_ADDR'=>'10.0.0.1']);
        $request->shouldReceive('getUri')->andReturn('/test-uri');
        $sessionData = [];

        $generator = new TokenGenerator($this->getSession($sessionData), $request);
        $token = $generator->getToken('/dest-uri');

        $renderer = new Renderer();

        $html = $renderer->render($token);
        $this->assertTrue(strpos($html, $token->getIndex())!==false);
        $this->assertTrue(strpos($html, $token->getFormTokenValue())!==false);
    }
}