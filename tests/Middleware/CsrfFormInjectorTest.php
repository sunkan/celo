<?php

namespace Celo\Tests\Middleware;

use Celo\Middleware\CsrfFormInjector;
use Celo\NativeSession;
use Celo\Renderer;
use Celo\SessionFactoryInterface;
use Celo\SessionInterface;
use Celo\TokenGenerator;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CsrfFormInjectorTest extends TestCase
{
    private function getRequest(string $uri = '/test-uri', ?array $body = null, bool $delete = false): ServerRequestInterface
    {
        $request = Mockery::mock(ServerRequestInterface::class);
        $request->shouldReceive('getServerParams')->andReturn(['REMOTE_ADDR'=>'10.0.0.1']);

        if (!is_null($body)) {
            if ($delete) {
                $uriMock = Mockery::mock(UriInterface::class);
                $uriMock->shouldReceive('__toString')->andReturn($uri);
                $uriMock->shouldReceive('withQuery')->andReturn($uriMock);

                $request->shouldReceive('getUri')->andReturn($uriMock);
                $request->shouldReceive('getQueryParams')->andReturn($body);
                $request->shouldReceive('getParsedBody')->andReturn([]);
                $request->shouldReceive('getMethod')->andReturn('DELETE');
                $request->shouldReceive('withQuery')->andReturn($request);
                $request->shouldReceive('withUri')->andReturn($request);
            } else {
                $request->shouldReceive('getMethod')->andReturn('POST');
                $request->shouldReceive('getParsedBody')->andReturn($body);
            }
        }
        if (!$delete) {
            $uriMock = Mockery::mock(UriInterface::class);
            $uriMock->shouldReceive('__toString')->andReturn($uri);
            $request->shouldReceive('getUri')->andReturn($uriMock);
        }


        return $request;
    }

    public function testHtmlFormInject()
    {
        $session = [];
        $sessionMock = new NativeSession($session);

        $sessionFactoryMock = $this->getMockForAbstractClass(SessionFactoryInterface::class);
        $requestMock = $this->getRequest();
        $generator = new TokenGenerator($sessionMock, $requestMock);

        $requestMock->shouldReceive('getAttribute')->andReturn($generator);

        $responseMock = Mockery::mock(ResponseInterface::class);
        $responseMock->shouldReceive('getHeaderLine')->andReturn('html');

        $bodyMock = Mockery::mock(StreamInterface::class);

        $bodyMock->shouldReceive('__toString')->andReturn(' <form method="post" action="/new_test_url/"></form>');
        $bodyMock->shouldReceive('rewind')->andReturnTrue();
        $bodyMock->shouldReceive('write')->withArgs(function($html) {
            $this->assertTrue((bool) strpos($html, '_CSRF_INDEX'));
            $this->assertTrue((bool) strpos($html, '_CSRF_TOKEN'));

            return true;
        })->andReturnTrue();

        $responseMock->shouldReceive('getBody')->andReturn($bodyMock);

        $handlerMock = Mockery::mock(RequestHandlerInterface::class);
        $handlerMock->shouldReceive('handle')->andReturn($responseMock);

        $middleware = new CsrfFormInjector(new Renderer(), $sessionFactoryMock);

        $middleware->process($requestMock, $handlerMock);
    }
}