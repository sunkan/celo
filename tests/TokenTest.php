<?php

namespace Celo\Tests;

use Celo\Token;
use PHPUnit\Framework\TestCase;

class TokenTest extends TestCase
{
    public function testJsonSerialize()
    {
        $uri = '/test';
        $lockTo = '/test/post';
        $token = Token::createToken($uri, $lockTo, null);

        $this->assertInstanceOf(Token::class, $token);

        $array = $token->jsonSerialize();

        $this->assertSame($uri, $array['uri']);
        $this->assertSame($lockTo, $array['lock_to']);

        try {
            $createTime = new \DateTime('@'.$array['created']);
            $this->assertInstanceOf(\DateTime::class, $createTime);
        } catch (\Exception $e) {
            $this->fail('Invalid create time');
        }

        $this->assertEquals(24, strlen($array['index']));
        $this->assertEquals(44, strlen($array['token']));
    }
}
