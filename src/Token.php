<?php

namespace Celo;

use ParagonIE\ConstantTime\Base64UrlSafe;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class Token implements TokenInterface
{
    private $index;
    private $created;
    private $token;
    private $uri;
    private $lockTo;
    private $formToken;

    /**
     * @param string $uri
     * @param string $lockTo
     * @param callable|null $hashToken
     * @return Token|TokenInterface
     * @throws \Exception
     */
    public static function createToken(string $uri, string $lockTo, ?callable $hashToken = null): TokenInterface
    {
        if (is_null($hashToken)) {
            $hashToken = function ($token) {
                return $token;
            };
        }

        $created = \intval(\date('YmdHis'));
        $index = Base64UrlSafe::encode(\random_bytes(18));
        $storeToken = Base64UrlSafe::encode(\random_bytes(33));
        $formToken = $hashToken($storeToken);

        return new Token($uri, $lockTo, $storeToken, $index, $created, $formToken);
    }

    public function __construct(string $uri, string $lockTo, string $storeToken, string $index, int $created, ?string $formToken = null)
    {
        $this->index = $index;
        $this->created = $created;
        $this->token = $storeToken;
        $this->formToken = $formToken;
        $this->uri = $uri;
        $this->lockTo = $lockTo;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getIndex(): string
    {
        return $this->index;
    }

    public function getCreated(): int
    {
        return $this->created;
    }

    public function getLockTo(): string
    {
        return $this->lockTo;
    }

    public function getFormIndex(): string
    {
        return TokenInterface::FORM_INDEX;
    }

    public function getFormToken(): string
    {
        return TokenInterface::FORM_TOKEN;
    }

    public function getFormTokenValue(): string
    {
        return $this->formToken;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
            return [
                'index' => $this->index,
                'uri' => $this->uri,
                'lock_to' => $this->lockTo,
                'token' => $this->token,
                'created' => $this->created
            ];
    }
}
