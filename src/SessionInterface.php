<?php

namespace Celo;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
interface SessionInterface
{
    public function gc(int $keepCount, ?callable $sort = null);
    public function add(TokenInterface $token);
    public function addLimited(TokenInterface $token, int $limit, int $expire);
    public function has(string$index): bool;

    /**
     * Gets token by id and deletes the token
     */
    public function retrieve(string $index): TokenInterface;
}
