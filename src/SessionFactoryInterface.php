<?php

namespace Celo;

use Psr\Http\Message\ServerRequestInterface;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
interface SessionFactoryInterface
{
    public function newInstance(ServerRequestInterface $request): SessionInterface;
}
