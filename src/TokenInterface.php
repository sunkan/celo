<?php

namespace Celo;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
interface TokenInterface extends \JsonSerializable
{
    public const FORM_INDEX = '_CSRF_INDEX';
    public const FORM_TOKEN = '_CSRF_TOKEN';

    public static function createToken(string $uri, string $lockTo, ?callable $hashToken = null): TokenInterface;

    public function getToken(): string;
    public function getIndex(): string;
    public function getCreated(): int;
    public function getLockTo(): string;

    public function getFormIndex(): string;
    public function getFormToken(): string;
    public function getFormTokenValue(): string;
}
