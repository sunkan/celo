<?php

namespace Celo;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class NativeSession implements SessionInterface
{
    /**
     * @var string
     */
    private $sessionIndex = 'CSRF';
    private $session;

    public function __construct(?array &$session = null)
    {
        $this->session = &$session ?? $_SESSION;
        $sessions = &$this->session;
        if (!isset($sessions[$this->sessionIndex])) {
            $sessions[$this->sessionIndex] = [];
        }
    }

    public function gc(int $keepCount, ?callable $sort = null)
    {
        if (is_null($sort)) {
            $sort = function ($a, $b): int {
                return (int) ($a['created'] <=> $b['created']);
            };
        }

        $sessions = &$this->session[$this->sessionIndex];
        if (\count($sessions) < $keepCount) {
            return;
        }

        // Sort by creation time
        \uasort(
            $sessions,
            $sort
        );

        while (\count($sessions) > $keepCount) {
            // Let's knock off the oldest one
            \array_shift($sessions);
        }
    }

    public function add(TokenInterface $token)
    {
        $index = $token->getIndex();
        $this->session[$this->sessionIndex][$index] = $token->jsonSerialize();
    }

    public function addLimited(TokenInterface $token, int $limit, int $expire)
    {
        $lockTo = $token->getLockTo();
        $lockKey = 'lock-'.md5($lockTo);

        $indexs = $this->session[$this->sessionIndex . $lockKey] ?? [];
        $indexs[] = $token->getIndex();

        if (count($indexs) > $limit) {
            $sessions = &$this->session[$this->sessionIndex];
            while (\count($indexs) > $limit) {
                // Let's knock off the oldest one
                \array_shift($sessions);
            }
        }
        $this->session[$this->sessionIndex . $lockKey] = $indexs;

        $this->add($token);
        $this->session[$this->sessionIndex][$token->getIndex()]['expire'] = $expire;
    }

    public function has(string $index): bool
    {
        if (isset($this->session[$this->sessionIndex][$index])) {
            if (isset($this->session[$this->sessionIndex][$index]['expire'])) {
                $time = \intval(\date('YmdHis')) - $this->session[$this->sessionIndex][$index]['created'];
                if ($this->session[$this->sessionIndex][$index]['expire'] >= $time) {
                    return true;
                }
                unset($this->session[$this->sessionIndex][$index]);
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Gets token by id and deletes the token
     */
    public function retrieve(string $index): TokenInterface
    {
        $data = $this->session[$this->sessionIndex][$index];

        unset($this->session[$this->sessionIndex][$index]);

        return new Token($data['uri'], $data['lock_to'], $data['token'], $index, $data['created']);
    }
}
