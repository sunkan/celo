<?php

namespace Celo;

use Celo\Exception\CorruptDataException;
use Celo\Exception\LockToException;
use Celo\Exception\MissingDataException;
use Celo\Exception\NotValidException;
use Celo\Exception\TokenNotFound;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\ConstantTime\Binary;
use Psr\Http\Message\ServerRequestInterface;

/**
 * A small CSRF library for generating/verifying CSRF tokens
 *
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 * @version    2.0.0
 */
class TokenGenerator
{
    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var string
     */
    protected $hashAlgo = 'sha256';

    /**
     * @var int
     */
    protected $recycleAfter = 65535;

    /**
     * @var int
     */
    protected $xhrLimit = 10;

    /**
     * @var bool
     */
    protected $hmacIp = true;

    /**
     * @var bool
     */
    protected $expireOld = false;

    /**
     * @var ServerRequestInterface
     */
    private $request;

    public function __construct(SessionInterface $session, ServerRequestInterface $request)
    {
        $this->session = $session;
        $this->request = $request;
    }

    private function _getToken(string $lockTo): TokenInterface
    {
        $hashClb = null;
        if ($this->hmacIp) {
            $server = ($request ?? $this->request)->getServerParams();
            $algo = $this->hashAlgo;
            $hashClb = function (string $token) use ($server, $algo) {
                return Base64UrlSafe::encode(
                    \hash_hmac(
                        $algo,
                        $server['REMOTE_ADDR'] ?? '127.0.0.1',
                        (string) Base64UrlSafe::decode($token),
                        true
                    )
                );
            };
        }

        return Token::createToken((string) ($request ?? $this->request)->getUri(), $this->cleanUrl($lockTo), $hashClb);
    }

    /**
     * @param string $lockTo
     * @return TokenInterface
     */
    public function getToken(string $lockTo): TokenInterface
    {
        $token = $this->_getToken($lockTo);
        $this->session->add($token);
        $this->recycleTokens();

        return $token;
    }

    /**
     * @param string $lockTo
     * @return TokenInterface
     */
    public function getLimitedToken(string $lockTo, int $expire): TokenInterface
    {
        $token = $this->_getToken($lockTo);
        $this->session->addLimited($token, $this->xhrLimit, $expire);
        $this->recycleTokens();

        return $token;
    }

    /**
     * Validate a request based on Session and ServerRequestInterface
     *
     * @param null|ServerRequestInterface $request
     * @return TokenInterface
     * @throws CorruptDataException
     * @throws LockToException
     * @throws MissingDataException
     * @throws NotValidException
     * @throws TokenNotFound
     * @throws \TypeError
     */
    public function validate(?ServerRequestInterface $request = null): TokenInterface
    {
        $request = $request ?? $this->request;
        $postBody = $request->getParsedBody();

        // DELETE don't allow post body
        if (empty($postBody) && $request->getMethod() === 'DELETE') {
            $newQuery = $postBody = $request->getQueryParams();

            //remove token info from query string
            unset($newQuery[TokenInterface::FORM_INDEX], $newQuery[TokenInterface::FORM_TOKEN]);
            $request = $request->withUri($request->getUri()->withQuery(http_build_query($newQuery)));
        }

        if (!isset($postBody[TokenInterface::FORM_INDEX]) || !isset($postBody[TokenInterface::FORM_TOKEN])) {
            // User must transmit a complete index/token pair
            throw new MissingDataException();
        }

        // Let's pull the POST data
        $index = $postBody[TokenInterface::FORM_INDEX];
        $token = $postBody[TokenInterface::FORM_TOKEN];

        if (!\is_string($index) || !\is_string($token)) {
            throw new CorruptDataException();
        }
        if (!$this->session->has($index)) {
            throw new TokenNotFound();
        }

        // Grab the value stored at $index
        $stored = $this->session->retrieve($index);

        // Which form action="" is this token locked to?
        $lockTo = $this->cleanUrl((string) $request->getUri());

        if (!\hash_equals($lockTo, $stored->getLockTo())) {
            // Form target did not match the request this token is locked to!
            throw new LockToException('Token not for this uri');
        }

        // This is the expected token value
        if ($this->hmacIp === false) {
            // We just stored it wholesale
            $expected = $stored->getToken();
        } else {
            $server = $request->getServerParams();
            // We mixed in the client IP address to generate the output
            $expected = Base64UrlSafe::encode(
                \hash_hmac(
                    $this->hashAlgo,
                    $server['REMOTE_ADDR'] ?: '127.0.0.1',
                    (string) Base64UrlSafe::decode($stored->getToken()),
                    true
                )
            );
        }
        if (!\hash_equals($token, $expected)) {
            throw new NotValidException();
        }
        return $stored;
    }

    /**
     * Enforce an upper limit on the number of tokens stored in session state
     * by removing the oldest tokens first.
     *
     * @return void
     */
    protected function recycleTokens(): void
    {
        if ($this->expireOld) {
            $this->session->gc($this->recycleAfter);
        }
    }

    /**
     * @param string $url
     * @return string
     * @throws \TypeError
     */
    private function cleanUrl(string $url)
    {
        //remove domain part from url
        if (stripos($url, '://')) {
            $data = parse_url($url);

            $url = isset($data['path']) ? $data['path'] : '';
            $url .= isset($data['query']) ? '?' . $data['query'] : '';
        }

        if (\preg_match('#/$#', $url)) {
            // Trailing slashes are to be ignored
            $url = Binary::safeSubstr(
                $url,
                0,
                Binary::safeStrlen($url) - 1
            );
        }
        return $url;
    }
}
