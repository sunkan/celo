<?php

namespace Celo\Exception;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class MissingDataException extends \Exception
{

}