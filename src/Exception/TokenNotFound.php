<?php

namespace Celo\Exception;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class TokenNotFound extends \Exception
{

}