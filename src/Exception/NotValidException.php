<?php

namespace Celo\Exception;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class NotValidException extends \Exception
{

}