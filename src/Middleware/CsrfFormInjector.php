<?php

namespace Celo\Middleware;

use Celo\Renderer;
use Celo\SessionFactoryInterface;
use Celo\TokenGenerator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class CsrfFormInjector implements MiddlewareInterface
{
    private $tokenRenderer;
    private $sessionFactory;

    public function __construct(Renderer $tokenRenderer, SessionFactoryInterface $sessionFactory)
    {
        $this->tokenRenderer = $tokenRenderer;
        $this->sessionFactory = $sessionFactory;
    }

    private function getGenerator(ServerRequestInterface $request): TokenGenerator
    {
        $csrf = $request->getAttribute('csrf');
        if ($csrf instanceof Csrf) {
            $generator = $csrf->getGenerator();
        } else {
            $generator = new TokenGenerator($this->sessionFactory->newInstance($request), $request);
        }

        return $generator;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        if (stripos($response->getHeaderLine('Content-Type'), 'html') !== false) {
            $generator = $this->getGenerator($request);
            $this->injectIntoHtml($response, function (?string $lockTo) use ($request, $generator) {
                $lockTo = $lockTo ?? (string) $request->getUri();
                return $generator->getToken($lockTo);
            });
        } elseif ($request->getMethod() !== 'GET' && stripos($response->getHeaderLine('Content-Type'), 'json') !== false) {
            $body = $response->getBody();
            $json = json_decode((string) $body, true);
            if ($json && is_array($json)) {
                $token = $this->getGenerator($request)->getLimitedToken((string) $request->getUri(), 600);
                $json['data']['csrf'] = [
                    'index' => [$token->getFormIndex(), $token->getIndex()],
                    'token' => [$token->getFormToken(), $token->getFormTokenValue()]
                ];

                $body->rewind();
                $body->write(json_encode($json));
            }
        }

        return $response;
    }

    protected function injectIntoHtml(ResponseInterface $response, callable $tokenGenerator)
    {
        $body = $response->getBody();
        $html = (string) $body;

        if (stripos($html, '<form') !== false) {
            $html = preg_replace_callback('#(<form[^>]*method\s*=\s*["\']post["\'][^>]*>)#i', function ($matches) use ($tokenGenerator) {
                preg_match('/\saction=[\'"]([\w\/-\=\&\?]+)[\'"]/i', $matches[0], $m);
                return $matches[0] . $this->tokenRenderer->render($tokenGenerator($m[1]));
            }, $html);

            $body->rewind();
            $body->write($html);
        }
    }
}
