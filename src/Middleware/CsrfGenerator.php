<?php

namespace Celo\Middleware;

use Celo\SessionFactoryInterface;
use Celo\TokenGenerator;
use Interop\Http\Factory\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class CsrfGenerator implements MiddlewareInterface
{
    private $sessionFactory;
    private $responseFactory;

    public function __construct(ResponseFactoryInterface $responseFactory, SessionFactoryInterface $sessionFactory)
    {
        $this->responseFactory = $responseFactory;
        $this->sessionFactory = $sessionFactory;
    }

    private function getGenerator(ServerRequestInterface $request): TokenGenerator
    {
        $generator = $request->getAttribute('csrf_generator');
        if (!$generator instanceof TokenGenerator) {
            $generator = new TokenGenerator($this->sessionFactory->newInstance($request), $request);
        }

        return $generator;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($request->hasHeader('Csrf-Generate-Token')) {
            $response = $this->responseFactory->createResponse(200);
            $token = $this->getGenerator($request)->getLimitedToken($request->getHeaderLine('Csrf-Generate-Token'), 5);
            $json = ['data' => ['csrf' => []]];
            $json['data']['csrf'] = [
                'index' => [$token->getFormIndex(), $token->getIndex()],
                'token' => [$token->getFormToken(), $token->getFormTokenValue()],
                'lockTo' => $token->getLockTo()
            ];

            $response->getBody()->write(json_encode($json));
            return $response;
        }
        return $handler->handle($request);
    }
}
