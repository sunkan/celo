<?php

namespace Celo\Middleware;

use Celo\SessionFactoryInterface;
use Celo\TokenGenerator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class CsrfValidate implements MiddlewareInterface
{
    private $sessionFactory;

    public function __construct(SessionFactoryInterface $sessionFactory)
    {
        $this->sessionFactory = $sessionFactory;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $tokenGenerator = new TokenGenerator($this->sessionFactory->newInstance($request), $request);

        if ($request->getMethod() !== 'GET') {
            try {
                $token = $tokenGenerator->validate();
                $csrf = new Csrf(true, $tokenGenerator, $token);
            } catch (\Exception $e) {
                $csrf = new Csrf(false, $tokenGenerator, $e);
            }
        } else {
            $csrf = new Csrf(true, $tokenGenerator, null);
        }

        return $handler->handle($request->withAttribute('csrf', $csrf));
    }
}
