<?php

namespace Celo\Middleware;

use Celo\TokenGenerator;
use Celo\TokenInterface;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class Csrf
{
    private $isValid;
    private $generator;
    private $token;
    private $exception;

    public function __construct(bool $isValid, TokenGenerator $generator, $data)
    {
        $this->generator = $generator;
        $this->isValid = $isValid;
        if ($data instanceof TokenInterface) {
            $this->token = $data;
        } elseif ($data instanceof \Exception) {
            $this->exception = $data;
        }
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->isValid;
    }

    /**
     * @return TokenGenerator
     */
    public function getGenerator(): TokenGenerator
    {
        return $this->generator;
    }

    /**
     * @return mixed
     */
    public function getToken(): ?TokenInterface
    {
        return $this->token;
    }

    /**
     * @return \Exception
     */
    public function getException(): ?\Exception
    {
        return $this->exception;
    }
}
