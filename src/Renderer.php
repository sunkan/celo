<?php

namespace Celo;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class Renderer
{
    public function render(TokenInterface $token): string
    {
        $indexArgs = [
            'type' => '"hidden"',
            'name' => '"'.$token->getFormIndex().'"',
            'value' => '"'.\htmlentities($token->getIndex(), ENT_QUOTES, 'UTF-8').'"'
        ];
        $tokenArgs = [
            'type' => '"hidden"',
            'name' => '"'.$token->getFormToken().'"',
            'value' => '"'.\htmlentities($token->getFormTokenValue(), ENT_QUOTES, 'UTF-8').'"'
        ];

        $return = [
            '<input '.urldecode(http_build_query($indexArgs, '', ' ')).' />',
            '<input '.urldecode(http_build_query($tokenArgs, '', ' ')).' />',
        ];

        return "\n".implode("\n", $return);
    }
}
