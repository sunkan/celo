<?php

namespace Celo;

use Aura\Session\Session;
use Aura\Session\Segment;

/**
 * @author     Andreas Sundqvist <andreas@sunkan.se>
 */
class AuraSession implements SessionInterface
{
    /**
     * @var Segment
     */
    protected $segment;

    public function __construct(Session $session)
    {
        $this->segment = $session->getSegment('CSRF');
    }

    public function gc(int $keepCount, ?callable $sort = null)
    {
        return;
    }

    public function add(TokenInterface $token)
    {
        $index = $token->getIndex();
        $this->segment->set($index, $token->jsonSerialize());
    }

    public function addLimited(TokenInterface $token, int $limit, int $expire)
    {
        $lockTo = $token->getLockTo();
        $lockKey = 'lock-'.md5($lockTo);
        $indexs = $this->segment->get($lockKey, []);
        $indexs[] = $token->getIndex();

        if (count($indexs) > $limit) {
            while (\count($indexs) > $limit) {
                // Let's knock off the oldest one
                $index = \array_shift($indexs);
                $this->segment->set($index, null);
            }
        }

        $this->segment->set($lockKey, $indexs);

        $index = $token->getIndex();
        $tokenData = $token->jsonSerialize();
        $tokenData['expire'] = $expire;
        $this->segment->set($index, $tokenData);
    }

    public function has(string $index): bool
    {
        $data = $this->segment->get($index);

        if (!empty($data)) {
            if (isset($data['expire'])) {
                if ($data['expire'] >= \intval(\date('YmdHis')) - $data['created']) {
                    return true;
                }
                $this->segment->set($index, null);
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Gets token by id and deletes the token
     */
    public function retrieve(string $index): TokenInterface
    {
        $data = $this->segment->get($index);
        $this->segment->set($index, null);

        return new Token($data['uri'], $data['lock_to'], $data['token'], $index, $data['created']);
    }
}
