# CSRF Library

## Inspiration

This library is heavily inspired by (https://github.com/paragonie/anti-csrf)

## Installation

The preferred method of installing this library is with
[Composer](https://getcomposer.org/) by running the following from your project
root:

  $ composer require sunkan/celo

## Using

This library is meant to be used in an application that utilizes **[Psr-15](https://www.php-fig.org/psr/psr-15/)** dispatcher implementation.

We include a couple of middlewares that make it easy to use

### Validate csrf
```php

$sessionFactory = new class() implements Celo\SessionFactoryInterface {
    public function newInstance(ServerRequestInterface $request): SessionInterface {
        return new Celo\NativeSession();
    }
};

$dispatcher = new PSR15Dispatcher();

// validates csrf token and set csrf attribute
$dispatcher->addMiddleware(new Celo\Middleware\CsrfValidate($sessionFactory));

$dispatcher->handle($request, function($request) {
    //fallback handler
    
    /** @var Celo\Middleware\Csrf $csrf */
    $csrf = $request->getAttribute('csrf);
    if ($csrf->isValid()) {
        echo "Valid request";
    } else {
        $csrf->getException();
    }
    
    $generator = $csrf->getGenerator();
    $newToken = $generator->getToken('/url-to-lock-token to');
});
```

### Auto inject token into forms and json responses
```php

$sessionFactory = new class() implements Celo\SessionFactoryInterface {
    public function newInstance(ServerRequestInterface $request): SessionInterface {
        return new Celo\NativeSession();
    }
};

$dispatcher = new PSR15Dispatcher();

// if response is html it will look for <form and add the correct input fields
// if the response is json and request method is not GET it will add a new token to the response data
$dispatcher->addMiddleware(new Celo\Middleware\CsrfFormInjector(new Celo\Renderer(), $sessionFactory);
```